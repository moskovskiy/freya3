import React from 'react'
import './tabs.css'

function Tabs(props) {
    //style={{width: ((100/arr.length - 2)+"%")}}
    return (
        <div className="tabs">
            {props.tabs.map((el, index, arr) => 
                <button onClick={()=>props.setIndex(index)} className={(index == props.index)?"tabs__tab tabs__tab--active":"tabs__tab"} key={index}>{el}</button>
            )}
        </div>
    )
}
  
export default Tabs;
  
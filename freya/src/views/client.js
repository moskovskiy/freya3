import React, { useEffect, useState, Component } from 'react'
import axios from 'axios'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Input from '../components/input/input'
import Row from '../components/row/row'
import Divider from '../components/divider/divider'
import Tabs from '../components/tabs/tabs'

import Iframe from 'react-iframe'
import logo from '../media/logo.png'
import QrReader from 'react-qr-reader'

function Client (props) {

    let id = props.match.params.id

    let [state, setState] = useState("auth")
    let [code, setCodeFx] = useState("")
    let [displayError, setError] = useState("")
    let [pointInfo, setPointInfo] = useState({})
    let [userInfo, setUserInfo] = useState({})
    let [actions, setActions] = useState([])
    let [qrError, setQrError] = useState("")
    let [motive, setMotive] = useState(0)
    let [summ, setSummFx] = useState("")
    let [camera, setCamera] = useState(true)

    function setSumm (e, max) {
        let sum = e.target.value
        if ((sum > max) && (motive == 1)) sum = max
        setSummFx(sum)
    }

    if ((localStorage.getItem('session-token') == null) || (localStorage.getItem('session-token') == "null") ) {
     window.location.href = "/" + id + "/auth"
    }
    //let [logo, setLogo] = useState({})

    function updatePointInfo () {
        axios.get(' https://api4.simployal.com/c/' + id + '/info')
        .then((response) => {
            setPointInfo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    function addVisit () {
        axios.post(' https://api4.simployal.com/p/' + id + '/pay', {
            code: code,
            amount: 1
        }, {
            headers: {
                Authorization: localStorage.getItem('session-token')
            }
        })
        .then((response) => {
            findClient(code)
            let a = [{action: "Зачислен 1 бонус"}]
            a.push(actions)
            setActions(a)
            alert("Успешно")
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    function cancelAddVisit () {
        axios.post(' https://api4.simployal.com/p/' + id + '/pay', {
            code: code,
            amount: -1
        }, {
            headers: {
                Authorization: localStorage.getItem('session-token')
            }
        })
        .then((response) => {
            findClient(code)
            let a = [{action: "Отмена зач 1 бонус"}]
            a.push(actions)
            setActions(a)
            alert("Успешно")
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    function removeVisits () {
        axios.post(' https://api4.simployal.com/p/' + id + '/pay', {
            code: code,
            amount: -1 * (pointInfo.type - 1)
        }, {
            headers: {
                Authorization: localStorage.getItem('session-token')
            }
        })
        .then((response) => {
            findClient(code)
            let a = [{action: "Списаны бонусы"}]
            a.push(actions)
            setActions(a)
            alert("Успешно")
            //setPointInfo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    function cancelRemoveVisits () {
        axios.post(' https://api4.simployal.com/p/' + id + '/pay', {
            code: code,
            amount: 1 * (pointInfo.type - 1)
        }, {
            headers: {
                Authorization: localStorage.getItem('session-token')
            }
        })
        .then((response) => {
            findClient(code)
            let a = [{action: "Отмена спис бонусы"}]
            a.push(actions)
            setActions(a)
            alert("Успешно")
            //setPointInfo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    function directOperation (value) {
        axios.post(' https://api4.simployal.com/p/' + id + '/pay', {
            code: code,
            amount: value
        }, {
            headers: {
                Authorization: localStorage.getItem('session-token')
            }
        })
        .then((response) => {
            findClient(code)
            let a = [{action: ("Операция на " + value)}]
            a.push(actions)
            setActions(a)
            alert("Успешно")
            //setPointInfo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })
    }

    useEffect(() => {
        updatePointInfo()
        /*axios.get(' https://api4.simployal.com/c/' + id + '/logo/1', {size: "small"})
        .then((response) => {
            alert(JSON.stringify(response.data))
            setLogo(response.data)
        })
        .catch((error) => {
            setError(error.message)
        })*/
    }, [userInfo])

    function setCode (e) {
        let code = e.target.value
        setError("")
        window.scroll(0, 300)
        setCodeFx(code)

        if (code.length >= pointInfo.code_length) {
            findClient(code)
        }

        code = code.substring(0, pointInfo.code_length)
    }

    function findClient (client_code) {
        axios.get(' https://api4.simployal.com/c/' + id + '/amount/' + client_code)
        .then((response) => {
            if (response.data.message="ok") {
                setState("client")
                setUserInfo(response.data)

            } else {
                setError(JSON.stringify(response.data.message))
            }
        })
        .catch(function (error) {
            setError(JSON.stringify(error.message))
        })
    }

    function QRError (msg) {
       setCamera(false)
    }

    function QRSuccess (msg) {
        if (msg != null) {
            setCodeFx(msg)
            findClient(msg)
        }
        setCamera(true)
    }

    const previewStyle = {
        height: "250px",
        width: "250px",
        marginLeft: "calc(50vw - 140px)"
    }
    //https://dikidi.net/244958?p=0.pi-po-sm&o=1
    return (
        <div className="application">
            {(state == "auth") && <> <img className="img" src={logo}/>
            <View header="" title="Готов к работе">
                
                {camera && <>
                    <Row nowrap={true} name="Отсканируйте QR"/>
                    <QrReader
                        delay={500}
                        style={previewStyle}
                        onError={QRError}
                        onScan={QRSuccess}
                    />
                </>}

                {(displayError != "")&& <>
                    <Divider/>
                    <Text text="Нет такого клиента или иная ошибка"></Text>
                    <Row nowrap="true" name={displayError}/>
                </>}

                <Divider/>
                <Input type="number" pattern="[0-9]*" scale="large" header="Или введите код вручную" onChange={setCode} value={code}  hint="123456" autoFocus={true}/>
                <Row nowrap={true} name="Для сканирования QR разрешите доступ к камере"/>
                <Row nowrap={true} name="Работает в Safari на iOS или Chrome на Android"/>
                
                
                {/*<Iframe url="https://dikidi.net/244958"
                        width="400px"
                        height="500px"
                        className="bookingAddr"
                        display="initial"
                    position="relative"/>*/}

                <Divider/>


            </View> </>}
            {(state == "client") && <> <img className="img" src={logo}/>
            <View header="" title="Клиент">
                <Row nowrap={true} name={"Клиент: " + userInfo.name + " (" + userInfo.code + ")"}/>
                <Text text="Текущий баланс: "/>
                <div style={{fontWeight: 900, fontSize: "25px"}}>{userInfo.amount}</div>
                
                <Divider/>
                <Tabs tabs={["Начислить", "Списать"]} index={motive} setIndex={setMotive}/>
                
                {(pointInfo.type != 0) &&<>
                    {(motive == 0) && <>
                        <Button color="#353DF6" backgroundColor="#D0EBFF" text="Добавить +1" onClick={() => addVisit()}/>
                        <Button color="#353DF6" backgroundColor="#D0EBFF" text="Отменить +1: ошибка" onClick={() => cancelAddVisit()}/>
                    </>}

                    {(motive == 1) && <>
                        <Button color="#353DF6" backgroundColor="#D0EBFF" text={"Cписать " + (pointInfo.type - 1) + " за беспл."} onClick={() => removeVisits()}/>
                        <Button color="#353DF6" backgroundColor="#D0EBFF" text={"Отменить и вернуть " + (pointInfo.type - 1)} onClick={() => cancelRemoveVisits()}/>
                    </>}
                </>}

                {(pointInfo.type == 0) &&<>
                    {(motive == 0) && <>
                        <Input  type="number" pattern="[0-9]*" scale="large" hint={"1000"} header={"Оплаченная сумма, " + pointInfo.percent + "% начислится"} value={summ} onChange={setSumm} autoFocus={true}/>
                        <Button color="#353DF6" backgroundColor="#D0EBFF" text="Посчитать % и начислить" onClick={() => directOperation((pointInfo.percent*summ/100), 10000000)}/>
                    </>}

                    {(motive == 1) && <>
                        <Input  type="number" pattern="[0-9]*" scale="large" hint={userInfo.amount} header="Сумма к списанию" value={summ} onChange={setSumm} autoFocus={true}/>
                        <Button color="#353DF6" backgroundColor="#D0EBFF" text={"Списать"} onClick={() => directOperation((-1*summ), userInfo.amount)}/>
                    </>}
                </>}

                <Divider/>
                <Button text="Следующий клиент" onClick={() => {
                    setCodeFx("")
                    setState("auth")
                    setActions([])
                    setMotive(0)
                    setSummFx("")
                }}/>
                <br/>
                {(displayError != "")&& <>
                    <Text text="Ошибка в действии"></Text>
                    <Row nowrap="true" name={displayError}/>
                </>}

                {actions.map((el) => 
                    <>
                        <Row nowrap="true" name={el.action}/>
                    </>
                )}
            </View> </>}
        </div>
    );
}

export default Client;

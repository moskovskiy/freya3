import React, { useEffect, useState } from 'react'
import axios from 'axios'
import View from '../components/view/view'
import Button from '../components/button/button'
import Text from '../components/text/text'
import Input from '../components/input/input'
import Row from '../components/row/row'

import logo from '../media/logo.png'

function Auth (props) {

    let id = props.match.params.id

    let [password, setPasswordFx] = useState("")
    let [displayError, setError] = useState("")

    function setPassword (e) {
        setPasswordFx(e.target.value)
        setError("")
    }

    function requestAuth () {
        axios.post(' https://api4.simployal.com/p/' + id + '/login', {
            password: password
        })
        .then((response) => {
            if (response.data.message="ok") {
                localStorage.setItem('session-token', response.data.token)
                window.location.href="/" + id + "/client"
            } else {
                setError(JSON.stringify(response.data.message))
            }
        })
        .catch(function (error) {
            setError(JSON.stringify(error.message))
        })
    }

    return (
        <div className="application">
            <img className="img" src={logo}/>
            <View header="Авторизация" title="Авторизация">
                <Input center={true} type="password" header="Введите пароль, чтобы продолжить" onChange={setPassword} value={password}  hint="Пароль"/>
                <Button text="Войти" onClick={()=>requestAuth()}/>
                {(displayError != "")&& <>
                    <Text text="Произошла ошибка авторизации, проверьте пароль"></Text>
                    <Row nowrap="true" name={displayError}/>
                </>}
            </View>
        </div>
    );
}

export default Auth;

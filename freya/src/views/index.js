import './index.css';
import React from 'react';
import { Route, BrowserRouter as Router, Switch, Redirect} from 'react-router-dom'
import Auth from './auth.js'
import Client from './client.js'
import '../fonts/index.css'

function App() {
  let link = "/auth"
  if ((localStorage.getItem('session-token') != null) && (localStorage.getItem('session-token') != "null") ) {
    link = "/client"
  }

  return (
    <div className="application">
      <Router>
      <Switch>
        <Route exact path="/" render={() => (<Redirect to={link} />)} /> 
        <Route exact path="/:id/client" component={Client}/>
        <Route exact path="/:id/auth" component={Auth}/>
      </Switch>
      </Router>
    </div>
  );
}

export default App;
